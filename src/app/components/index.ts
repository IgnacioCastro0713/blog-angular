export * from './error/error.component';
export * from './home/home.component';
export * from './auth/login/login.component';
export * from './auth/register/register.component';
export * from './auth/settings-profile/settings-profile.component';
